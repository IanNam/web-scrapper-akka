package kr.iannam.scraper.actor

import java.net.URI

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.routing.RoundRobinPool
import kr.iannam.scraper.actor.`trait`.CrawlerLike
import kr.iannam.scraper.actor.model.Crawler.execute
import kr.iannam.scraper.actor.model.Partitioner.sendSeedUrls
import kr.iannam.scraper.actor.model.Scraper
import kr.iannam.scraper.actor.model.Scraper.scrapingDocument
import kr.iannam.scraper.item.URLTable
import org.jsoup.Jsoup
import org.jsoup.select.Elements

import scala.collection.JavaConversions._

class Crawler extends Actor with CrawlerLike with ActorLogging {

  val MAX_DEPTH = 5
  var currentDepth = 0

  val system: ActorSystem = context.system
  val scraper: ActorRef = system.actorOf(Scraper.props)

  override def receive: Receive = {
    case execute(url) => {
//      if (MAX_DEPTH >= currentDepth) {
        val hrefList = getCrawableURLList(url)
        sendSeedRequest(hrefList, sender())

        val routingPoolSize = getRoutingPoolSize(hrefList.length)
        val scraperAgents = system.actorOf(RoundRobinPool(getRoutingPoolSize(routingPoolSize)).props(Scraper.props))
        for (url: String <- hrefList) {
          if (URLTable.getURL(url).equals("")) {
            sendScrapRequest(url, scraperAgents)
            URLTable.addURL(url)
          }
        }
        log.info(s"<<< DEPTH = $currentDepth")
        currentDepth += 1
//      } else {
//        afterAll()
//      }
    }
  }

  override def getCrawableURLList(url: String): List[String] = {
    val domainName = new URI(url).getHost
    try {
      val jsoupConn = Jsoup.connect(url)
      jsoupConn.timeout(5 * 1000)

      val document = jsoupConn.get()
      val links: Elements = document.getElementsByTag("a")
      links.map(x => transformURL(x.attr("href"), domainName)).toList
    } catch {
      case e: Exception => {
        println(s"${e.getMessage} $url is unreachable, Maybe not crawable!!!")
        URLTable.addUnreachableURL(url)
        List.empty[String]
      }
    }
  }

  override def sendSeedRequest(seed: List[String], actor: ActorRef): Unit =
    actor ! sendSeedUrls(seed)

  override def sendScrapRequest(url: String, actor: ActorRef): Unit =
    actor ! scrapingDocument(url)

  override def afterAll(): Unit =
    system.terminate()

  def getRoutingPoolSize(seedNumber: Int, n: Int = 5): Int =
    if (seedNumber < n) seedNumber else n

  def transformURL(url: String, domainName: String): String =
    "((http:\\/\\/|https:\\/\\/).*)".r
      .findAllIn(url).toArray.size match {
      case 0 => domainName + url
      case _ => url
    }

  def printURLTable(): Unit =
    URLTable.getAllURLs().foreach(println)
}