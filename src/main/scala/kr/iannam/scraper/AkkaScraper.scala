
package kr.iannam.scraper

import akka.actor.{ActorRef, ActorSystem}
import kr.iannam.scraper.actor.model.Partitioner
import kr.iannam.scraper.actor.model.Partitioner.sendSeedUrls
import com.google.gson._
import kr.iannam.scraper.model.SeedURL

import scala.util.matching.Regex

object AkkaScraper {
  val seedUrls: List[String] = List(
    "http://time.com/section/us/",
    "http://time.com/section/politics/",
    "http://time.com/section/world/",
    "http://time.com/section/business/",
    "http://time.com/section/tech/",
    "http://time.com/section/health/"
  )

  def main(args: Array[String]): Unit = {
    val json_string = scala.io.Source.fromFile(getClass.getResource("/rules/crawling_rule.json").getPath).getLines.mkString
    val jsonStringAsObject: JsonObject = new JsonParser().parse(json_string).getAsJsonObject

    val ruleData = jsonStringAsObject.getAsJsonArray("seed_urls")
    val seedURLs = ruleData.iterator().forEachRemaining(x => {
      val seed = x.getAsJsonObject
      val url = seed.get("url").getAsJsonObject
      val thread = seed.get("max_thread").getAsInt
      val depth = seed.get("depth").getAsInt
      val label = seed.get("label").getAsString
      val prefix = url.get("prefix").getAsString
      val rules = url.get("rules").getAsJsonObject

      val ruleRegex: Regex = "\\{(.*?)\\}".r
      val matches = ruleRegex.findAllIn(prefix).matchData.toList
        .map(_.toString().replace("{", "").replace("}", ""))

      var resultURL: List[String] = List.empty[String]
      matches.map(x => {
        val rule = rules.get(x).getAsString
        if (rule.contains(",")) {
          val ls = rule.split(",")
            .map(_.trim).toList
            .map(prefix.replace(s"{$x}", _))
          resultURL = resultURL ::: ls
        } else {
          if (resultURL.size > 1) {
            resultURL = resultURL.map(_.replace(s"{$x}", rule))
          } else {
            resultURL = prefix.replace(s"{$x}", rule) :: resultURL
          }
        }
      })
//      startCrawler(SeedURL(thread, depth, label, resultURL))
    })
  }


  def startCrawler(seedURLs: SeedURL): Unit = {
    val system: ActorSystem = ActorSystem("AkkaScraper")
    val seedParitioner: ActorRef = system.actorOf(Partitioner.props, "partitioner_actor")
//    seedParitioner ! sendSeedUrls(seedURLs)
  }
}
