package kr.iannam.scraper.item

import scala.collection.mutable

object URLTable {
  val urlTable: mutable.HashMap[String, String] = mutable.HashMap.empty[String, String]
  val unreachableURLs: mutable.HashMap[String, String] = mutable.HashMap.empty[String, String]

  def sha256Hash(text: String): String = String.format("%064x", new java.math.BigInteger(1, java.security.MessageDigest.getInstance("SHA-256").digest(text.getBytes("UTF-8"))))

  def addURL(url: String): Unit = urlTable ++= Map(sha256Hash(url) -> url)
  def removeURL(url: String): Unit = urlTable ++= Map(sha256Hash(url) -> url)
  def getURL(url: String): String = urlTable.getOrElse(sha256Hash(url), "")

  def addUnreachableURL(url: String): Unit = unreachableURLs ++= Map(sha256Hash(url) -> url)
  def getAllURLs(): mutable.HashMap[String, String] = urlTable
  def getUnreachableURL(): mutable.HashMap[String, String] = unreachableURLs
}
