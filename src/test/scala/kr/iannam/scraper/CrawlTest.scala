package kr.iannam.scraper

import com.google.gson.{JsonObject, JsonParser}
import org.scalatest.FlatSpec

import scala.util.matching.Regex

class CrawlTest extends FlatSpec {

  "Given url path" should "have valid host and path values" in {
//    val section = "economy, world-economy, us-economy, finance, banks, investing, wall-street, mergers-and-acquisitions, politics, congress, law, taxes, consumers, autos"
//      .split(",").map(_.trim).toList
//    val targetPage = 0
//    val prefix = "https://www.cnbc.com/%s/?page=%d"
//    getCrawlingURL(prefix, section, targetPage = 1).foreach(println)

    parseRuleData()
  }


  def parseRuleData(): Unit = {
    val json_string = scala.io.Source.fromFile(getClass.getResource("/rules/crawling_rule.json").getPath).getLines.mkString
    val jsonStringAsObject: JsonObject = new JsonParser().parse(json_string).getAsJsonObject

    val ruleData = jsonStringAsObject.getAsJsonArray("seed_urls")
    ruleData.iterator().forEachRemaining(x => {
      val rules = x.getAsJsonObject

      // parse rule
      val pools = rules.get("pools").getAsString
      val label = rules.get("label").getAsString

      // parse crawling_rule
      val crawlingRule = rules.get("crawling_rule").getAsJsonObject
      val prefix = crawlingRule.get("prefix").getAsString
      val args = crawlingRule.get("args").getAsJsonObject
      val section = args.get("section").getAsString
      val startPage = args.get("start_page").getAsInt

      // parse scraping_rule
      val scrapingRule = rules.get("scraping_rule").getAsJsonObject
      val link = scrapingRule.get("link").toString
      val title = scrapingRule.get("title").toString
      val datetime = scrapingRule.get("datetime").toString
      val content = scrapingRule.get("content").toString

      val sectionList = section.split(",").map(_.trim).toList
      val seedURLs = sectionList.map(prefix.format(_, startPage))
      letsCrawling(seedURLs, startPage)
    })
  }

  def letsCrawling(section: List[String], targetPage: Int): Unit = {
    section.foreach(println)
  }
}