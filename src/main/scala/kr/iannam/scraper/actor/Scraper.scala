package kr.iannam.scraper.actor

import java.io.{File, FileOutputStream, PrintWriter}

import akka.actor.{Actor, ActorLogging}
import com.google.gson.Gson
import kr.iannam.scraper.actor.`trait`.ScraperLike
import kr.iannam.scraper.actor.model.Scraper.scrapingDocument
import kr.iannam.scraper.item.{DocData, URLTable}
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class Scraper extends Actor with ScraperLike with ActorLogging {
  val OUTPUT_PREFIX = "D:\\Projects\\AkkaScraper\\output\\"

  override def receive: Receive = {
    case scrapingDocument(url) => {
      log.info(s"[FROM: ${sender().path.name}], [TO: ${self.path.name}] $url")
      try {
        val jsoupConn = Jsoup.connect(url)
        jsoupConn.timeout(5 * 1000)

        val document: Document = jsoupConn.get()

        val pw = new PrintWriter(new File(OUTPUT_PREFIX + URLTable.sha256Hash(url)) + ".json")

        val key = URLTable.sha256Hash(url)
        val data = Jsoup.parse(document.toString).text()
        val gson = new Gson()
        val jsonData = gson.toJson(DocData(key, url, data))
        pw.write(jsonData)

        val tableMap = new File(OUTPUT_PREFIX + "URLMappingTable.txt")
        if(tableMap.exists()) {
          new PrintWriter(new FileOutputStream(new File(OUTPUT_PREFIX + "URLMappingTable.txt"), true))
            .append(s"${URLTable.sha256Hash(url)}, ${url}\n")
            .close()
        } else {
          new PrintWriter(OUTPUT_PREFIX + "URLMappingTable.txt")
            .append(s"${URLTable.sha256Hash(url)}, ${url}")

            .close()
        }
      } catch {
        case e: Exception => {
          println(s"${e.getMessage} ${URLTable.sha256Hash(url)} ${URLTable.getURL(url)}")
        }
      }
    }
  }
}