package kr.iannam.scraper

import org.scalatest.FlatSpec

class Main extends FlatSpec {

  "A Stack" should "pop values in last-in-first-out order" in {

    val urls = "https://www.foxbusiness.com/category/{1}?{2}"
    val conditions = "economy, world-economy, us-economy, finance, banks, investing, wall-street, mergers-and-acquisitions, politics, congress, law, taxes, consumers, autos"
    val pages = "page=100"

    val conditionList = conditions.split(",").map(_.trim).toList

    conditionList.map(urls.replace("{1}", _)).foreach(println)
  }
}