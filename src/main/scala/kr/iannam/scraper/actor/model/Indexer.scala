package kr.iannam.scraper.actor.model

import kr.iannam.scraper.actor.Indexer
import akka.actor.Props

object Indexer {
  def props: Props = Props[Indexer]
}
