package kr.iannam.scraper.actor

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.routing.RoundRobinPool
import kr.iannam.scraper.AkkaScraper.seedUrls
import kr.iannam.scraper.actor.`trait`.PartitionerLike
import kr.iannam.scraper.actor.model.Crawler
import kr.iannam.scraper.actor.model.Crawler.execute
import kr.iannam.scraper.actor.model.Partitioner.sendSeedUrls
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class Partitioner extends Actor with PartitionerLike with ActorLogging {

  val system: ActorSystem = context.system

  implicit val timeout = Timeout(5 seconds)
  implicit val ec = context.system.dispatcher

  override def receive: Receive = {
    case sendSeedUrls(seedURL) =>
//      val routingPoolSize = (seedURL.maxThread)
//      val crawlerAgents = system.actorOf(RoundRobinPool(routingPoolSize).props(Crawler.props))
//
//      for (url <- seedURL.urls) {
//        val future = crawlerAgents ? execute(url)
//        future onComplete  {
//          case Success(result) =>
//          case Failure(ex) =>
//        }
//      }
  }
}

