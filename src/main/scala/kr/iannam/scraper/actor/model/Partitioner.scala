package kr.iannam.scraper.actor.model

import akka.actor.Props
import kr.iannam.scraper.actor.Partitioner

object Partitioner {
  def props: Props = Props[Partitioner]
  final case class sendSeedUrls(seedURLs: List[String])
}
