### AkkaScraper 는...
### 주어진 Seed Url List로부터 Recursive하게 Web 을 Crawling 하고, 원하는 정보를 Scraping 해서 json으로 저장하는 프로그램입니다.<br/>

- Akka 기반으로 동시성, 멀티 노드를 통한 병렬 Crawling/Scraping 기능을 제공합니다.
- 뉴스/피드 document처럼 링크가 노출되어있는 페이지에 적합합니다.
- 중요한건 아직 개발 진행 중입니다.........<br/><br/>

하단의 아키텍쳐 참조
https://stash.daou.co.kr/projects/OS/repos/akkascraper/browse/architecture.png <br/>

[남상욱 주임연구원](mailto:ian.nam@daou.co.kr)