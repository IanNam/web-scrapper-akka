package kr.iannam.scraper.item

case class DocData(key: String, url: String, data: String)