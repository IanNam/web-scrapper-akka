package kr.iannam.scraper.actor.model

import kr.iannam.scraper.actor.Crawler
import akka.actor.Props

object Crawler {
  def props: Props = Props[Crawler]
  final case class execute(urls: String)

  case object scrape
  case object write
}
