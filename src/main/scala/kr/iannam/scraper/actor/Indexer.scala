package kr.iannam.scraper.actor

import akka.actor.{Actor, ActorLogging}
import kr.iannam.scraper.actor.model.Indexer

class Indexer extends Actor with ActorLogging {

  override def receive: Receive = {
    case Indexer => log.info(s"${sender()}")
  }
}