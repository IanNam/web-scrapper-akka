package kr.iannam.scraper.actor.model

import kr.iannam.scraper.actor.Frontier
import akka.actor.Props

object Frontier {
  def props: Props = Props[Frontier]
  final case class getPageToCrawl(url: String)
}
