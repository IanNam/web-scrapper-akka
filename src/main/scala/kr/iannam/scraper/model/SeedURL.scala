package kr.iannam.scraper.model

case class SeedURL(maxThread: Int, depth: Int, label: String, urls: List[String])
