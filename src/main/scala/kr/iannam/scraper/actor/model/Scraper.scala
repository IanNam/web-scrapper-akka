package kr.iannam.scraper.actor.model

import kr.iannam.scraper.actor.Scraper
import akka.actor.Props

object Scraper {
  def props: Props = Props[Scraper]
  final case class scrapingDocument(url: String)
}
