package kr.iannam.scraper.actor.`trait`

import akka.actor.ActorRef

trait CrawlerLike {
  def getCrawableURLList(seed: String): List[String]
  def sendSeedRequest(seed: List[String], actor: ActorRef)
  def sendScrapRequest(url: String, actor: ActorRef)
  def afterAll()
}
